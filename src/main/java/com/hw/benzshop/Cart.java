/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hw.benzshop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BenZ
 */
public class Cart {

    private static ArrayList<Product> cartList = new ArrayList<>();

    public static boolean addProduct(Product product) {
        cartList.add(product);
        save();
        return true;
    }

    public static ArrayList<Product> getProduct() {
        return cartList;
    }

    public static Product getProduct(int index) {
        return cartList.get(index);
    }
    
    public static boolean delProduct(int index) {
        cartList.remove(index);
        save();
        return true;
    }
    public static boolean delAllProduct() {
        cartList.clear();
        save();
        return true;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("benz.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(cartList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cart.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cart.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    public static void lode() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("benz.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            cartList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Cart.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Cart.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Cart.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

    }

}
