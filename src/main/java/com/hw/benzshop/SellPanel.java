/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hw.benzshop;

import java.awt.HeadlessException;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 *
 * @author BenZ
 */
public class SellPanel extends javax.swing.JPanel {

    /**
     * Creates new form SellPanel
     */
    private MainFrame mainFrame;
    private DefaultListModel model, modelCart;

    public SellPanel(MainFrame mainFrame) {

        initComponents();
        btnEdit1.setVisible(false);
        pnlCart.setVisible(false);
        HideBtnProduct();
        ProductManagement.lode();
        this.mainFrame = mainFrame;
        model = new DefaultListModel();
        lstProduct.setModel(model);
        refresh();

    }

    public void totalPrice() {
        int indexCart = Cart.getProduct().size();
        int amountCart, sumAllAmount = 0;
        double priceCart, sumAllPrice = 0;

        for (int i = 0; i < indexCart; i++) {
            amountCart = Cart.getProduct(i).getAmount();
            priceCart = Cart.getProduct(i).getPrice();
            sumAllPrice += priceCart * amountCart;
            sumAllAmount += amountCart;
        }//"%.2f", d
        txtAllAmount.setText(sumAllAmount + "");
        txtAllPrice.setText(String.format("%.2f", sumAllPrice));;
    }

    public void HideBtnProduct() {
        btnAddCart.setVisible(false);
        edtAmount.setVisible(false);
        txtSelectName4.setVisible(false);
        txtSelectPrice.setVisible(false);
        txtSelectName3.setVisible(false);
        txtSelectName.setVisible(false);
        txtSelectName2.setVisible(false);
    }

    public void HideBtnCart() {
        txtSelectName5.setVisible(false);
        txtEditName.setVisible(false);
        txtSelectName6.setVisible(false);
        edtEditAmount.setVisible(false);
        btnDel1.setVisible(false);
        btnDel.setVisible(false);

    }

    public void ShowBtnCart() {
        txtSelectName5.setVisible(true);
        txtEditName.setVisible(true);
        txtSelectName6.setVisible(true);
        edtEditAmount.setVisible(true);
        btnDel1.setVisible(true);
        btnDel.setVisible(true);

    }

    public void ShowBtnProduct() {
        btnAddCart.setVisible(true);
        edtAmount.setVisible(true);
        txtSelectName4.setVisible(true);
        txtSelectPrice.setVisible(true);
        txtSelectName3.setVisible(true);
        txtSelectName.setVisible(true);
        txtSelectName2.setVisible(true);
    }

    public void refresh() {
        model.clear();
        model.addAll(ProductManagement.getProduct());
    }

    public void refresh2() {
        modelCart.clear();
        modelCart.addAll(Cart.getProduct());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        pnlStock = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstProduct = new javax.swing.JList<>();
        btnAddCart = new javax.swing.JButton();
        txtSelectName = new javax.swing.JLabel();
        txtSelectPrice = new javax.swing.JLabel();
        edtAmount = new javax.swing.JTextField();
        txtSelectName2 = new javax.swing.JLabel();
        txtSelectName3 = new javax.swing.JLabel();
        txtSelectName4 = new javax.swing.JLabel();
        btnSelect1 = new javax.swing.JButton();
        pnlCart = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstCart = new javax.swing.JList<>();
        txtSelectName1 = new javax.swing.JLabel();
        txtAllAmount = new javax.swing.JLabel();
        txtAllPrice = new javax.swing.JLabel();
        txtEditName = new javax.swing.JLabel();
        btnEdit = new javax.swing.JButton();
        btnDel = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        edtEditAmount = new javax.swing.JTextField();
        txtSelectName5 = new javax.swing.JLabel();
        txtSelectName6 = new javax.swing.JLabel();
        txtSelectName7 = new javax.swing.JLabel();
        txtSelectName8 = new javax.swing.JLabel();
        btnDel1 = new javax.swing.JButton();
        btnEdit1 = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("ข้อมูลสินค้า");

        lstProduct.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(lstProduct);

        btnAddCart.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnAddCart.setText("เพิ่มลงในตะกร้า");
        btnAddCart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddCartActionPerformed(evt);
            }
        });

        txtSelectName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtSelectPrice.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        edtAmount.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        txtSelectName2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSelectName2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtSelectName2.setText("ชื่อสินค้า :");

        txtSelectName3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSelectName3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtSelectName3.setText("ราคา :");

        txtSelectName4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSelectName4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtSelectName4.setText("จำนวน :");

        btnSelect1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSelect1.setText("เลือกสินค้า");
        btnSelect1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelect1ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("ตะกร้าสินค้า");

        jScrollPane2.setViewportView(lstCart);

        txtSelectName1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtAllAmount.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtAllAmount.setText("aaa");

        txtAllPrice.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtEditName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        btnEdit.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnEdit.setText("แก้ไขตะกร้าสินค้า");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnDel.setText("ลบ");
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnClear.setText("ลบทั้งหมด");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jButton3.setText("ชำระเงิน");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        edtEditAmount.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        txtSelectName5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSelectName5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtSelectName5.setText("ชื่อสินค้า :");

        txtSelectName6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSelectName6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtSelectName6.setText("จำนวน :");

        txtSelectName7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSelectName7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtSelectName7.setText("จำนวนสินค้าทั้งหมด :");

        txtSelectName8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSelectName8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtSelectName8.setText("ราคารวม :");

        btnDel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnDel1.setText("แก้ไขจำนวน");
        btnDel1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDel1ActionPerformed(evt);
            }
        });

        btnEdit1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnEdit1.setText("แก้ไขบิลก่อนหน้า");
        btnEdit1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEdit1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlCartLayout = new javax.swing.GroupLayout(pnlCart);
        pnlCart.setLayout(pnlCartLayout);
        pnlCartLayout.setHorizontalGroup(
            pnlCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlCartLayout.createSequentialGroup()
                .addGroup(pnlCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnlCartLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(32, 32, 32)
                        .addComponent(txtSelectName5, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtEditName, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSelectName6, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(7, 7, 7)
                        .addComponent(edtEditAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnDel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(2, 2, 2)
                        .addComponent(btnDel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlCartLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtSelectName1, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(119, 119, 119)
                        .addGroup(pnlCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pnlCartLayout.createSequentialGroup()
                                .addComponent(txtSelectName7, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtAllAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtSelectName8, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlCartLayout.createSequentialGroup()
                                .addComponent(btnEdit1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnEdit)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtAllPrice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)))
                    .addGroup(pnlCartLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 745, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(63, 63, 63))
        );
        pnlCartLayout.setVerticalGroup(
            pnlCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCartLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtSelectName6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtSelectName5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtEditName, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlCartLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(pnlCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edtEditAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnDel)
                                .addComponent(btnClear)
                                .addComponent(btnDel1)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlCartLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtAllAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtSelectName7)
                            .addComponent(txtSelectName8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtAllPrice, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlCartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton3)
                            .addComponent(btnEdit)
                            .addComponent(btnEdit1))
                        .addGap(16, 16, 16))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlCartLayout.createSequentialGroup()
                        .addComponent(txtSelectName1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(31, 31, 31))))
        );

        javax.swing.GroupLayout pnlStockLayout = new javax.swing.GroupLayout(pnlStock);
        pnlStock.setLayout(pnlStockLayout);
        pnlStockLayout.setHorizontalGroup(
            pnlStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlStockLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(pnlStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnAddCart)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 738, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlStockLayout.createSequentialGroup()
                        .addComponent(txtSelectName2, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtSelectName, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSelectName3, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtSelectPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSelectName4, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(edtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSelect1, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(pnlStockLayout.createSequentialGroup()
                .addGroup(pnlStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(pnlCart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 2, Short.MAX_VALUE))
        );
        pnlStockLayout.setVerticalGroup(
            pnlStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlStockLayout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(txtSelectName, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSelectName2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtSelectName3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtSelectPrice)
                    .addComponent(txtSelectName4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(edtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSelect1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAddCart)
                .addGap(18, 18, 18)
                .addComponent(pnlCart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(pnlStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(pnlStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddCartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddCartActionPerformed
        if (insertCart()) {
            refresh2();
            //txtSelectName
            //totalPrice();
            pnlCart.setVisible(true);
            HideBtnCart();
            txtSelectName.setText("");
            txtSelectPrice.setText("");
            edtAmount.setText("");
            totalPrice();
        }


    }//GEN-LAST:event_btnAddCartActionPerformed

    public boolean insertCart() throws NumberFormatException, HeadlessException {
        //pnlCart.setVisible(true);
        int index = lstProduct.getSelectedIndex();
        Product productSl = ProductManagement.getProduct(index);
        String name = productSl.getName();
        String id = productSl.getId();
        String bland = productSl.getBland();
        double price = productSl.getPrice();
        int amount = Integer.parseInt(edtAmount.getText());;

        if (amount < 1) {
            JOptionPane.showMessageDialog(null, "Please enter a number greater than 0.");
            return false;
        }

        if (amount <= productSl.getAmount()) {
            Product cart = new Product(id, name, bland, price, amount);
            Cart.addProduct(cart);
            modelCart = new DefaultListModel();
            lstCart.setModel(modelCart);
            productSl.setAmount(productSl.getAmount() - amount);
            refresh();
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Not enough products.");
            return false;
        }

    }

    private void btnSelect1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelect1ActionPerformed

        int index = lstProduct.getSelectedIndex();
        //System.out.println(index);
        if (index < 0) {
            return;
        }
        Product product = ProductManagement.getProduct(index);
        txtSelectName.setText(product.getName());
        txtSelectPrice.setText(product.getPrice() + "");
        edtAmount.setText("1");
        edtAmount.requestFocus();
        ShowBtnProduct();
    }//GEN-LAST:event_btnSelect1ActionPerformed

    private void btnDel1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDel1ActionPerformed

        /* 
        int index = lstCart.getSelectedIndex();
        int amount =Integer.parseInt(edtEditAmount.getText());
        Cart.getProduct(index).setAmount(amount);
        refresh2();
        totalPrice();
         */
        int index = lstCart.getSelectedIndex();
        String id = Cart.getProduct(index).getId();
        int inStock = 0;
        int indexProduct = -1;
        int size = ProductManagement.getProduct().size();
        for (int i = 0; i < size; i++) {
            if (ProductManagement.getProduct(i).getId().equals(id)) {
                inStock = ProductManagement.getProduct(i).getAmount()
                        + Cart.getProduct(index).getAmount();
                indexProduct = i;
                i = size;
            }
        }

        int amount = Integer.parseInt(edtEditAmount.getText());

        if (amount <= inStock) {

            Cart.getProduct(index).setAmount(amount);
            refresh2();
            totalPrice();
            ProductManagement.getProduct(indexProduct).setAmount(inStock - amount);
            refresh();
        } else {

            JOptionPane.showMessageDialog(null, "Not enough products.");
            return;
        }
    }//GEN-LAST:event_btnDel1ActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        ProductManagement.lode();
        refresh();
        Cart.delAllProduct();
        refresh2();
        resetEdit();
        totalPrice();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        /*int index = lstCart.getSelectedIndex();
        Cart.delProduct(index);
        refresh2();
        resetEdit();
        totalPrice();
         */
        int index = lstCart.getSelectedIndex();
        String id = Cart.getProduct(index).getId();
        int size = ProductManagement.getProduct().size();
        for (int i = 0; i < size; i++) {
            if (ProductManagement.getProduct(i).getId().equals(id)) {
                int o =ProductManagement.getProduct(i).getAmount();
                ProductManagement.getProduct(i).
                        setAmount(Cart.getProduct(index).getAmount()+o);
                Cart.delProduct(index);
                refresh2();
                refresh();
                resetEdit();
                totalPrice();
                i = size;
            }
        }


    }//GEN-LAST:event_btnDelActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int index = lstCart.getSelectedIndex();
        //System.out.println(index);
        if (index < 0) {
            return;
        }
        ShowBtnCart();
        txtEditName.setText(Cart.getProduct(index).getName());
        edtEditAmount.setText(Cart.getProduct(index).getAmount() + "");
        edtEditAmount.requestFocus();

    }//GEN-LAST:event_btnEditActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        int reply = JOptionPane.showConfirmDialog(null,
                "Do you want to pay all?", "Wraning", JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            Cart.save();
            modelCart.clear();
            txtAllAmount.setText("");
            txtAllPrice.setText("");
            btnEdit1.setVisible(true);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btnEdit1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEdit1ActionPerformed
       
        //refresh();
        Cart.lode();
        refresh2();
        totalPrice();
    }//GEN-LAST:event_btnEdit1ActionPerformed

    public void resetEdit() {
        txtEditName.setText("");
        edtEditAmount.setText("");
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddCart;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnDel1;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnEdit1;
    private javax.swing.JButton btnSelect1;
    private javax.swing.JTextField edtAmount;
    private javax.swing.JTextField edtEditAmount;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList<String> lstCart;
    private javax.swing.JList<String> lstProduct;
    private javax.swing.JPanel pnlCart;
    private javax.swing.JPanel pnlStock;
    private javax.swing.JLabel txtAllAmount;
    private javax.swing.JLabel txtAllPrice;
    private javax.swing.JLabel txtEditName;
    private javax.swing.JLabel txtSelectName;
    private javax.swing.JLabel txtSelectName1;
    private javax.swing.JLabel txtSelectName2;
    private javax.swing.JLabel txtSelectName3;
    private javax.swing.JLabel txtSelectName4;
    private javax.swing.JLabel txtSelectName5;
    private javax.swing.JLabel txtSelectName6;
    private javax.swing.JLabel txtSelectName7;
    private javax.swing.JLabel txtSelectName8;
    private javax.swing.JLabel txtSelectPrice;
    // End of variables declaration//GEN-END:variables
}
