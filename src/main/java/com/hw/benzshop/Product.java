/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hw.benzshop;

/**
 *
 * @author BenZ
 */
public class Product {
    private String id , name, bland;
    private double price;
    private int amount;

    public Product(String id, String name, String bland, double price, int amount) {
        this.id = id;
        this.name = name;
        this.bland = bland;
        this.price = price;
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBland() {
        return bland;
    }

    public void setBland(String bland) {
        this.bland = bland;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "ID : "+id+"   Name :  "+name+"   Bland :  "+bland+"   Price(Bath) "
                + ":  "+price+"   Amount :  "+amount;
    }
    
    
    
}
